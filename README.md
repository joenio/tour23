# live coding tour 2023

- https://tour23.4two.art

## tools to render maps

- Leaflet https://leafletjs.com/ ++ smaller and new
- MapLibre https://maplibre.org/
- OpenLayers https://openlayers.org/

## authors

- Joenio Marques da Costa
- Mari Moura
